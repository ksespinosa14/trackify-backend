const User = require('../models/User');
const bcrypt = require("bcrypt");
const auth = require('../auth');
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require('google-auth-library');
const clientId = '664598190717-tke5be2abdqoaogklsbbm5tlghve20td.apps.googleusercontent.com';
const nodemailer = require('nodemailer');
const uuid1 = require('uuid1');


// Check if username exits
module.exports.userNameExists = (params) => {
    return User.find({ userName: params.userName }).then(resultFromFind => { return resultFromFind.length > 0 ? true : false })
};

// Check if email exits
module.exports.emailExists = (params) => {
    return User.find({ email: params.email }).then(resultFromFind => { 
        return resultFromFind.length > 0 ? true : false 
    })
};

// User registration
module.exports.register = (params) => {
    let newUser = new User({
    	userName: params.userName,
        email: params.email,
        password: bcrypt.hashSync(params.password, 10),
        loginType: 'email'
    })

    return newUser.save().then((user, err) => {
        if(err) return false
        else {
        	const transporter = nodemailer.createTransport({
	            host: 'smtp.gmail.com',
	            port: 465,
	            secure: true,
	            auth: {
	                user: 'trackifyph@gmail.com',
	                pass: 'trackify123'
	            }
	        });
	        // create a token for the email verification URL
        	jwt.sign(
        		{userId: user._id}, 
        		process.env.SECRET,
        		{
        			expiresIn: '1d'
        		},
        		(err, emailToken) => {
        			const url = `https://trackify-two.vercel.app/confirmation?token=${emailToken}`;

        			transporter.sendMail({
                        from: 'trackifyph@gmail.com',
                        to: user.email,
                        subject: 'Trackify Email Confirmation',
                        html: `Please click on the following link to confirm your registration <br /> <a href="${url}">${url}</a>`
                    });
                    // (error, info) => {
                    // 	console.log(err);
                    // });
        		}
        	)

        	return true;
        }
    })
};

// email verification
module.exports.verifyEmail = (token) => {
	const { userId } = jwt.verify(token, process.env.SECRET);
	const updates = {
		verified: true
	}

	return User.findByIdAndUpdate(userId, updates).then((verified, err) => {
        if(err || verified === null){
           return false; 
        }

        return true;
	})
};

// login
module.exports.login = (params) => {
    return User.findOne({ email: params.email }).then(resultFromFindOne => {
        if (resultFromFindOne === null) {
            return { error: 'does-not-exist' }
        }

        if (resultFromFindOne.loginType !== 'email') {
            return { error: 'login-type-error' }
        }

        if (resultFromFindOne.verified === false){
            return { error: 'unverified-email' }
        }

        const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)


        if (isPasswordMatched) {
            console.log(isPasswordMatched);
            return {
                accessToken: auth.createAccessToken(resultFromFindOne.toObject())
            }
        } else {
            return { error: 'incorrect-password' }
        }
    })
};

// get user details
module.exports.get = (params) => {
    return User.findById(params.userId,{password: 0}).then(user => {
        return user;
    })
};

// verify google token
module.exports.verifyGoogleTokenId = async (tokenId) => {

    const client = new OAuth2Client(clientId);
    const data = await client.verifyIdToken({
        idToken: tokenId,
        audience: clientId
    })

    console.log(data.payload)

    if (data.payload.email_verified === true) {
        const user = await User.findOne({ email: data.payload.email }).exec();

        if (user !== null) {
            if (user.loginType === "google") {
                return { accessToken: auth.createAccessToken(user.toObject()) }
            } else {
                return { error: "login-type-error" }
            }
        } 
        else {
            const newUser = new User({
                userName: data.payload.email.substring(0, data.payload.email.lastIndexOf("@")),
                email: data.payload.email,
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                loginType: "google",
                verified: true
            })

            return newUser.save().then((user, err)=>{
                return { accessToken: auth.createAccessToken(user.toObject()) }
            })
        }

    } else {
       return { error: "google-auth-error" }
    }
}

// add new transaction
module.exports.addTransaction = (params) => {
    return User.findById(params.userId).then(user => {
        user.transactions.push(params.transaction);
        return user.save().then((transaction, err) => {
            return (err) ? false : true;
        });

    });
}

// add new category
module.exports.addCategory = (params) => {
    return User.findById(params.userId).then(user => {
        const duplicates = user.categories.some(category => category.Title.toLowerCase() === params.Title.toLowerCase());
        if(duplicates){
            return { error: 'duplicate-category'};
        }
        else{
            const newCategory = []
            user.categories.push({
                Type: params.Type,
                Title: params.Title
            });

            user.categories.sort(function(a, b){
                if(a.Title.toLowerCase() < b.Title.toLowerCase()) { return -1; }
                if(a.Title.toLowerCase() > b.Title.toLowerCase()) { return 1; }
                return 0;
            });

            console.log(user.categories);
            return user.save().then((category, err) => {
                return (err) ? false : true;
            });
        }
    });
}

module.exports.updateUserInfo = (params) => {
    const updates = {
        userName: params.userName,
        firstName: params.firstName,
        lastName: params.lastName,
        mobileNo: params.mobileNo
    }

    return User.findByIdAndUpdate(params.userId, updates).then((user, err) => {
        return (err) ? false : true
    })
}

module.exports.updateUserPassword = (params) => {
    const newPassword = {
        password: bcrypt.hashSync(params.password, 10)
    }

    return User.findByIdAndUpdate(params.userId, newPassword).then((user, err) => {
        return (err) ? false : true
    })
}

module.exports.forgotPassword = (params) => {

    return User.find({ email: params }).then(resultFromFind => { 
        
        if(resultFromFind.length === 0) return false;
        else if(resultFromFind[0].loginType === 'google') return {error : 'login-type-google'}

        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'trackifyph@gmail.com',
                pass: 'trackify123'
            }
        });



        // create a token for the email verification URL
        const resetRequest = jwt.sign(
            {resetId: uuid1.UUID1()},  
            process.env.SECRET,
            {
                expiresIn: '1h'
            },
            {}
        )

        const url = `https://trackify-two.vercel.app/forgot?token=${resetRequest}`;

        transporter.sendMail({
            from: 'trackifyph@gmail.com',
            to: params,
            subject: 'Trackify Password Reset',
            html: `Please click on the following link to reset your password <br /> <a href="${url}">${url}</a>
                    <br />This will expire in 1 hour. Please ignore if you didn't make this reset request.`
        });

        return User.findByIdAndUpdate(resultFromFind[0]._id, { resetRequest: resetRequest } ).then((user, err) => {
            return (err) ? false : true
        })

    })

}

module.exports.resetPassword = (params) => {
    const { resetId } = jwt.verify(params.resetId, process.env.SECRET);
    if(resetId === undefined) return {error: 'expired-token'};

    return User.find({ resetRequest: params.resetId }).then(user => {
        if(user.length === 0) return {error: 'invalid-token'};

        const newPassword = {
            password: bcrypt.hashSync(params.password, 10)
        }

        return User.findByIdAndUpdate(user[0]._id, newPassword).then((user, err) => {
            return (err) ? false : true
        })

    });
}

