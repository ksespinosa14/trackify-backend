const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: [true, 'User name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    mobileNo: {
        type: String
    },
    password: {
        type: String,
        // required: [true, 'Password is requred.']
    },
    loginType: {
        type: String,
        required: [true, 'Login type is required.']
    },
    verified: {
        type: Boolean,
        default: false
    },
    resetRequest: {
        type: String
    },
    categories: {
        type: Array,
        default: [
            { 'Type': 'Expense', 'Title': 'Food' },
            { 'Type': 'Expense', 'Title': 'Housing' },
            { 'Type': 'Income', 'Title': 'Investments' },
            { 'Type': 'Expense', 'Title': 'Medical' },
            { 'Type': 'Income', 'Title': 'Salary' },
            { 'Type': 'Income', 'Title': 'Savings' },
            { 'Type': 'Expense', 'Title': 'Transportation' },
            { 'Type': 'Expense', 'Title': 'Utilities & Phone' }
        ]
    },
    transactions:[
        {
            title: {
                type: String,
                required: [true, 'Title is required.']
            },
            transactionType: {
                type: String,
                required: [true, 'Transaction type is required.']
            },
            category:{
                type: String,
                required: [true, 'Category is required.']
            },
            amount:{
                type: Number,
                required: [true, 'Amount ID is required.']
            },
            date: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, {timestamps: true});
userSchema.index({createdAt: 1},{expireAfterSeconds: 86400,partialFilterExpression : {verified: false}});

module.exports = mongoose.model('user', userSchema);