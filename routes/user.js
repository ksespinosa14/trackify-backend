const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user');
const auth = require('../auth');
const User = require('../models/User');


// Check if userName exits
router.post('/username-exists', (req, res) => {
    UserController.userNameExists(req.body).then(resultFromUsernameExists => res.send(resultFromUsernameExists))
});

// Check if email exits
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
});


// User registration
router.post('/', (req, res) => {
    UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
});

// Verify email
router.put('/confirmation/:token', (req, res) => {
	const token = req.params.token;
	UserController.verifyEmail(token).then(resultFromVerification => res.send(resultFromVerification));
});

// Login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
});;

// Retrieve User Details
router.get('/details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(resultFromDetails => res.send(resultFromDetails));
});


// Verify Google Token
router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

// Add transaction
router.post('/transaction', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		transaction: {
			title: req.body.name,
			transactionType: req.body.type,
			category: req.body.category,
			amount: req.body.amount
		}
	}
	UserController.addTransaction(params).then(resultsFromAddTransaction => res.send(resultsFromAddTransaction));
});

// Add category
router.post('/category', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		Type: req.body.type,
		Title: req.body.name
	}
	UserController.addCategory(params).then(resultsFromAddCategory => res.send(resultsFromAddCategory));
});

// update user info
router.put('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		userName: req.body.userName,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}
	UserController.updateUserInfo(params).then(resultsFromUdateUser => res.send(resultsFromUdateUser));
});

// update user password
router.put('/password', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		password: req.body.password
	}
	UserController.updateUserPassword(params).then(resultsFromUdatePassword => res.send(resultsFromUdatePassword));
});


// forgot password
router.post('/forgot', (req, res) => {
	UserController.forgotPassword(req.body.email).then(resultsFromForgotPassword => res.send(resultsFromForgotPassword));
});

// reset password
router.put('/reset', (req,res) => {
	const params = {
		password: req.body.password,
		resetId: req.body.resetId
	}
	UserController.resetPassword(params).then(resultsFromReset => res.send(resultsFromReset));
});

module.exports = router;